/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

    /***************************************************************************
     *                                                                          *
     * Default policy for all controllers and actions (`true` allows public     *
     * access)                                                                  *
     *                                                                          *
     ***************************************************************************/

    '*': ['sessionAuth', 'apiCall'],

    HomeController: true,

    AuthController: {
        '*': ['apiCall', 'sessionNotAuth'],
        logout: ['apiCall', 'sessionAuth'],
        get: ['apiCall', 'sessionAuth']
    },
    UserController: {
        '*': ['apiCall', 'sessionAuth', 'isAdmin'],
        find: ['apiCall', 'sessionAuth'],
        findOne: ['apiCall', 'sessionAuth'],
        chipsQuery: ['apiCall', 'sessionAuth'],
        all: ['apiCall', 'sessionAuth'],
        create: ['apiCall'],
        update: ['apiCall', 'sessionAuth', 'userUpdate']
    },
    RoleController: {
        '*': ['apiCall', 'sessionAuth', 'isAdmin']
    },
    //TODO: Move restrictions for events to policies as it belongs here instead of controller
    EventController: {
        '*': ['apiCall', 'sessionAuth']
    },
    ToolsController: {
        '*': ['apiCall']
    }
};
