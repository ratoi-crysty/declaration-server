'use strict';

/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function (cb) {

    // It's very important to trigger this callback method when you are finished
    // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

    // Create default admin if no user is in DB

    let ADMIN_ALREADY_EXISTS = 'ADMIN_ALREADY_EXISTS';

    User.find().then(function (users) {
        if (users.length) {
            let err = new Error('Admin already exists');
            err.error = ADMIN_ALREADY_EXISTS;
            throw err;
        }

        sails.log.info('Create admin user');

        return User.create({
            email: 'admin@email.local',
            password: 'admin',
            firstName: 'Admin',
            lastName: 'Admin'
        });
    }).then(function (user) {
        return Role.create({
            'type' : sails.config.roles.admin,
            'name' : 'admin',
            'user' : user.id
        })
    }).catch(function (err) {
        if (err.error === ADMIN_ALREADY_EXISTS) {
            return;
        }

        sails.log.error(err);
    }).finally(cb);
};
