'use strict';

var passport        = require('passport')
    , LocalStrategy = require('passport-local').Strategy;

/**
 * Serialize user into session
 */
passport.serializeUser(function (user, done) {
    // Save the user.id in the session
    done(null, user.id);
});

/**
 * Build user from session data
 */
passport.deserializeUser(function (id, done) {
    User
        .findOne({id: id, deletedAt: null})
        .populate('roles')
        .exec(function (err, user) {
            // Returns user false in case user is not in database anymore
            // This way it invalidates the current user session
            done(err, user || false);
        });
});

/**
 * Create local strategy for user login
 */
passport.use(new LocalStrategy({usernameField: 'email'}, function (email, password, done) {
    User
        .findOne({email: email, deletedAt: null})
        .populate('roles')
        .exec(function (err, user) {
            if (err) {
                return done(err);
            }

            if (!user) {
                return done(null, false, {message: 'Incorrect User'});
            }

            user.verifyPassword(password).then(function (status) {
                if (!status) {
                    return done(null, false, {message: 'Invalid Password'});
                }

                return done(null, user);
            }).catch(done);
        });
}));

/**
 * Register passport to application
 *
 * @type {{http: {customMiddleware: Function}}}
 */
module.exports = {
    passport: passport,
    http: {
        'customMiddleware': function (app) {
            console.log('express midleware for passport');
            app.use(passport.initialize());
            app.use(passport.session());
        }
    }
};
