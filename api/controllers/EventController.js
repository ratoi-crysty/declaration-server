'use strict';

/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Promise = require('bluebird')
    , path  = require('path')
    , fs    = Promise.promisifyAll(require('fs-extra'));

var eventFilesBaseFolder = path.join(__dirname, '..', '..', 'data', 'event-files');

exports._config = { actions: false, rest: false, shortcuts: false };

exports.find = function (req, res) {
    Event.find({'user': req.user.id}).populate('from').then(function (events) {
        for (var i = 0; i < events.length; i++) {
            events[i].data = JSON.parse(events[i].data);
        }
        res.ok(events);
    }).catch(res.negotiate);
};

exports.create = function (req, res) {
    var userId = req.param('to');
    var type = req.param('type');
    var description = req.param('description');
    var data = req.param('data');
    var files = req.param('files') || {};

    var filePaths = {};
    var event;

    Event.create({
        type: type,
        description: description,
        data: JSON.stringify(data),
        from: req.user.id,
        user: userId
    }).then(function (_event) {
        event = _event;
        if (!Object.keys(files).length) {
            // No files to save
            return true;
        }

        var ensureFiles = [];
        for (var x in files) {
            if (!files.hasOwnProperty(x)) {
                continue;
            }

            filePaths[x] = path.join(eventFilesBaseFolder, event.id.toString(), x);
            ensureFiles.push(fs.ensureFileAsync(filePaths[x]));
        }

        return Promise.all(ensureFiles);
    }).then(function () {
        var writeFiles = [];
        for (var x in files) {
            if (!files.hasOwnProperty(x)) {
                continue;
            }

            writeFiles.push(fs.writeFileAsync(filePaths[x], new Buffer(files[x], 'base64')));
        }
        return Promise.all(writeFiles);
    }).then(function () {
        res.ok(event);
    }).catch(res.negotiate);
};

exports.getFile = function (req, res) {
    var id = req.param('id');
    var file = req.param('file');
    var filePath = path.join(eventFilesBaseFolder, id, file);

    fs.readFileAsync(filePath).then(function (body) {
        res.ok({
            'file': body.toString('base64')
        });
    }).catch(function (err) {
        if (err.code == 'ENOENT') {
            err = new Error('File was not found');
            err.error = 'FILE_NOT_FOUND';
        }
        res.badRequest(err);
    });
};

exports.destroy = function (req, res) {
    let event;
    let folderPath;

    Event.findOne({id: req.param('id'), user: req.user.id}).then(function (_event) {
        event = _event;
        if (!event) {
            return res.notFound();
        }

        return Event.destroy({id: event.id});
    }).then(function () {
        folderPath = path.join(eventFilesBaseFolder, event.id.toString());

        return fs.remove(folderPath);
    }).then(function () {
        res.ok(event);
    }).catch(function (err) {
        res.serverError(err);
    });
};
