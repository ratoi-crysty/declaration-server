'use strict';

var validator   = require('validator')
    , passport  = require('passport');


/**
 * Attempts to login an user
 */
exports.login = function (req, res, next) {
    var BAD_CREDENTIALS = 'BAD_CREDENTIALS';

    passport.authenticate('local', function(err, user, info) {
        if (err) {
            return res.serverError(err);
        }
        if (!user) {
            err = new Error('Bad credentials');
            err.error = BAD_CREDENTIALS;
            return res.badRequest(err);
        }
        req.logIn(user, function(err) {
            if (err) {
                return res.serverError(err);
            }
            return res.ok(user);
        });
    })(req, res, next);
};

exports.logout = function (req, res) {
    req.logout();

    res.ok({
        description: 'User logged out'
    });
};

exports.get = function (req, res) {
    res.ok(req.user);
};
