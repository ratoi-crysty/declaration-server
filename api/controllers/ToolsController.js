'use strict';

/**
 * ToolsController
 *
 * @description :: Server-side logic for managing tools
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment');

exports.getServerTime = function (req, res) {
    var now = moment();

    res.ok({
        unix: now.unix(),
        offset: now.utcOffset()
    });
};
