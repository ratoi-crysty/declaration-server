/**
 * HomeController
 *
 * @description :: Server-side logic for managing Indices
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

exports.index = function (req, res) {
    res.view('home/index');
};
