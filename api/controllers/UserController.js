'use strict';

/**
 * UserController
 *
 *  TODO: Do not allow user to change or delete the public key
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let moment = require('moment');

exports.chipsQuery = function (req, res) {
    var query = req.param('query');

    var parts = query.split(' ');

    var conditions = [];

    for (var i = 0; i < parts.length; i++) {
        conditions.push({firstName: {contains: parts[i]}});
        conditions.push({lastName: {contains: parts[i]}});
        conditions.push({email: {contains: parts[i]}});
    }

    var where = {or: conditions, publicKey: {'!': null}};

    User.find(where).then(function (users) {
        res.ok(users);
    }).catch(function (err) {
        res.negotiate(err);
    });
};

exports.update = function (req, res) {
    let NOT_FOUND = 'NOT_FOUND';
    let INVALID_PASSWORD = 'INVALID_PASSWORD';

    let user;
    let roles;

    User.findOne({id: req.param('id')}).populate('roles').then(function (_user) {
        user = _user;

        if (!user) {
            let err = new Error('Not found');
            err.error = NOT_FOUND;
            throw err;
        }

        if (req.user.isAdmin()) {
            return true;
        } else if (user.publicKey && !req.param('publicKey')) {
            return user.verifyPassword(req.param('password'));
        } else {
            return true;
        }

    }).then(function (passwordValidation) {
        if (!passwordValidation) {
            let err = new Error('Invalid credentials');
            err.error = INVALID_PASSWORD;
            throw err;
        }

        var allowedParams = [
            'address',
            'city',
            'county',
            'postalCode'
        ];

        if (req.user.isAdmin()) {
            allowedParams = allowedParams.concat([
                'firstName',
                'lastName',
                'email',
                'publicKey'
            ]);
        }

        if (!user.publicKey) {
            user.publicKey = req.param('publicKey');
        }

        for (let i = 0; i < allowedParams.length; i++) {
            user[allowedParams[i]] = req.param(allowedParams[i]);
        }

        if (req.user.isAdmin() && user.publicKey === '') {
            user.publicKey = null;
        }

        if (req.param('newPassword')) {
            return user.updatePassword(req.param('newPassword'));
        } else {
            return true;
        }
    }).then(function () {
        roles = JSON.parse(JSON.stringify(user.roles));

        return user.save();
    }).then(function () {
        let output = JSON.parse(JSON.stringify(user));

        output.roles = roles;

        res.ok(output);
    }).catch(function (err) {
        if (err.error === NOT_FOUND) {
            return res.notFound();
        } else if (err.error === INVALID_PASSWORD || err.code === 'E_VALIDATION') {
            return res.badRequest(err);
        }

        res.serverError(err);
    });
};

exports.all = function (req, res) {
    var query = {};

    if (req.param('includeDeleted') !== 'true') {
        query.deletedAt = null;
    }

    User.find(query).populate('roles').then(function (users) {
        res.ok(users);
    }).catch(function (err) {
        res.serverError(err);
    });
};

exports.disable = function (req, res) {
    let NOT_FOUND = 'NOT_FOUND';
    let INVALID_PASSWORD = 'INVALID_PASSWORD';

    let user;
    let roles;

    User.findOne({id: req.param('id')}).then(function (_user) {
        user = _user;

        if (!user) {
            let err = new Error('Not found');
            err.error = NOT_FOUND;
            throw err;
        }

        if (req.user.isAdmin()) {
            return true;
        } else {
            return user.verifyPassword(req.param('password'));
        }

    }).then(function (passwordValidation) {
        if (!passwordValidation) {
            let err = new Error('Invalid credentials');
            err.error = INVALID_PASSWORD;
            throw err;
        }

        user.deletedAt = moment();

        roles = JSON.parse(JSON.stringify(user.roles));

        return user.save();

    }).then(function () {
        let output = JSON.parse(JSON.stringify(user));

        output.roles = roles;

        res.ok(output);
    }).catch(function (err) {
        if (err.error === NOT_FOUND) {
            return res.notFound();
        } else if (err.error === INVALID_PASSWORD) {
            return res.badRequest(err);
        }

        res.serverError(err);
    });
};
