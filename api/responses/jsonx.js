'use strict';

var _ = require('lodash');

module.exports = function jsonx(data) {
// Get access to `req`, `res`, & `sails`
    var req = this.req;
    var res = this.res;

    // Send conventional status message if no data was provided
    // (see http://expressjs.com/api.html#res.send)
    if (_.isUndefined(data)) {
        return res.status(res.statusCode).send();
    }
    else if (typeof data !== 'object') {
        // (note that this guard includes arrays)
        return res.send(data);
    }

    // When responding with an Error instance, if it's going to get sringified into
    // a dictionary with no `.stack` or `.message` properties, add them in.
    if (data instanceof Error) {
        var jsonSerializedErr;
        try {
            jsonSerializedErr = JSON.parse(JSON.stringify(data));
            if (!jsonSerializedErr.stack || !jsonSerializedErr.message) {
                jsonSerializedErr.message = data.message;
                jsonSerializedErr.stack = data.stack;
            }
            data = jsonSerializedErr;
        }
        catch (e) {
            data = {message: data.message, stack: data.stack};
        }

        delete data.stack;
    }

    if (req.options.jsonp && !req.isSocket) {
        return res.jsonp(data);
    }
    else return res.json(data);
};
