/**
 * sessionAuth
 *
 * @module      :: Policylogin
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function (req, res, next) {
    var USER_NOT_AUTHENTICATED = 'USER_NOT_AUTHENTICATED';

    if(req.isSocket && req.session && req.session.passport && req.session.passport.user)
    {
        // Initialize Passport
        sails.config.passport.initialize()(req, res, function () {
            // Use the built-in sessions
            sails.config.passport.session()(req, res, function () {
                // Make the user available throughout the frontend
                //res.locals.user = req.user;
                //the user should be deserialized by passport now;

                res.locals.user = req.user;

                next();
            });
        });
    }
    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    else if (req.user) {
        res.locals.user = req.user;

        next();
    } else {
        var err = new Error('User is not authenticated');
        err.error = USER_NOT_AUTHENTICATED;
        // User is not allowed
        return res.forbidden(err);
    }
};
