 'use strict';

var util = require('util');

module.exports = function (req, res, next) {

    if (!req.user.isAdmin()) {
        sails.log.warn(util.format(
            'Attempt to access route "%s" without admin role for user: id: %d; email: %s; name: %s %s',
            req.path,
            req.user.id,
            req.user.email,
            req.user.firstName,
            req.user.lastName
        ));

        return res.notFound();
    }

    next();
};
