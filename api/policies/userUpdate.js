'use strict';

var _ = require('lodash');

module.exports = function (req, res, next) {
    const ACCESS_DENIED = 'ACCESS_DENIED';
    if (!req.user.isAdmin() && req.params && req.params.id && parseInt(req.params.id) !== req.user.id) {
        let err = new Error('Access denied!');
        err.error = ACCESS_DENIED;
        sails.log.debug(req.params.id);
        return res.forbidden(err);
    }

    if (!req.params) {
        req.params = {};
    }

    if (req.params.id) {
        req.params.id = req.user.id;
    }

    next();
};
