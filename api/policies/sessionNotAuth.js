'use strict';

module.exports = function (req, res, next) {
    var USER_IS_AUTHENTICATED = 'USER_IS_AUTHENTICATED';

    if (req.user) {
        var err = new Error('User must be logged out');
        err.error = USER_IS_AUTHENTICATED;
        return res.forbidden(err);
    }

    return next();
};
