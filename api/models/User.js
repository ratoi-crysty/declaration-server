'use strict';

/**
 * User.js
 *
 * @description :: The user of the application, a user is required in order to use the aplication
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var Promise     = require('bluebird')
    , bcrypt    = require('bcrypt');

var salt;

/**
 * Generate hash from password
 *
 * @param password
 */
function generateHashPassword(password)
{
    var genSaltAsync = Promise.promisify(bcrypt.genSalt);
    var hashAsync = Promise.promisify(bcrypt.hash);

    var saltPromise;
    if (salt) {
        saltPromise = Promise.resolve(salt);
    } else {
        saltPromise = genSaltAsync(10);
    }

    return saltPromise.then(function (generatedSalt) {
        salt = generatedSalt;

        return hashAsync(password, salt);
    }).catch(function (err) {
        throw err;
    });
}

module.exports = {

    attributes: {
        firstName : {
            type : 'string',
            size : 50,
            required: true
        },
        lastName : {
            type : 'string',
            size : 30,
            required: true
        },
        address: {
            type: 'string',
            size: 255
        },
        city: {
            type: 'string',
            size: 100
        },
        county: {
            type: 'string',
            size: 25
        },
        postalCode: {
            type: 'string',
            size: 6
        },
        email : {
            type : 'email',
            unique: true,
            size : 150,
            required: true
        },
        password : {
            type : 'string',
            size : 100,
            required: true
        },
        publicKey : {
            type : 'text'
        },
        deletedAt : {
            type : 'datetime',
            defaultsTo : null
        },
        roles : {
            collection : 'role',
            via : 'user'
        },
        events : {
            collection : 'event',
            via : 'user'
        },
        sentEvents : {
            collection : 'event',
            via : 'from'
        },
        /**
         * Validate if password match
         *
         * @param password
         */
        verifyPassword : function (password) {
            let compare = Promise.promisify(bcrypt.compare);

            return compare(password, this.password);
        },

        updatePassword : function (password) {
            var self = this;
            return generateHashPassword(password).then(function (hash) {
                self.password = hash;
            });
        },

        /**
         * Deletes password from user when serialised into JSON
         *
         * @returns {*}
         */
        toJSON : function () {
            var obj = this.toObject();
            delete obj.password;

            return obj
        },

        /**
         * Verify if the user has a specific role
         *
         * @param role -- role type
         * @returns {boolean}
         */
        hasRole : function (role) {
            if (!this.roles) {
                throw new Error('An attempt was made to read the role without being loaded!');
            }

            for (var i = 0; i < this.roles.length; i++) {
                if (this.roles[i].type == role) {
                    return true;
                }
            }

            return false;
        },

        /**
         * Verify if user has Admin role
         *
         * @returns {boolean}
         */
        isAdmin : function () {
            return this.hasRole(sails.config.roles.admin);
        },

        /**
         * Disable the user
         *
         * @param callback
         * @returns {Promise.<this|Errors.ValidationError>}
         */
        disable : function (callback) {
            this.deletedAt = new Date;
            return this.save(callback);
        },

        /**
         * Enable the user
         *
         * @param callback
         * @returns {Promise.<this|Errors.ValidationError>}
         */
        enable : function (callback) {
            this.deletedAt = null;
            return this.save(callback);
        }
    },
    /**
     * Before insert into database, encrypt password
     *
     * @param user
     * @param cb
     */
    beforeCreate: function(user, cb) {
        generateHashPassword(user.password).then(function (hash) {
            user.password = hash;
            cb(null, user);
        }).catch(cb);
    },
    /**
     * Before insert into database, encrypt password
     *
     * @param user
     * @param cb
     */
    afterCreate: function(user, cb) {
        Role.create({
            'type' : sails.config.roles.user,
            'name' : 'user',
            'user' : user.id
        }).exec(cb);
    },
    /**
     * Add listener for soft deletes
     * In case the user is deleted, do not return it
     *
     * @param values
     * @param cb
     */
    beforeFind: function (values, cb) {
        if (values.isDelete === undefined) {
            values.isDelete = false;
        }

        cb();
    }
};

