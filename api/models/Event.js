/**
 * Event.js
 *
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    attributes: {
        type: {
            type: "string",
            size: 20,
            required: true
        },
        description: {
            type: "string",
            size: 255,
            required: true
        },
        data: "text",
        user : {
            model : "user",
            required: true,
            index: true
        },
        from : {
            model : "user",
            required: true,
            index: true
        }
    }
};

