'use strict';

/**
 * Role.js
 *
 * @description :: The roles that a user can have (E.g. normal user, admin, etc)
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    attributes : {
        name : {
            type : "string",
            size : 20
        },
        type : "integer",
        user : {
            model : "user",
            required: true,
            index: true
        }
    },
    autoCreatedAt : false,
    autoUpdatedAt : false
};

